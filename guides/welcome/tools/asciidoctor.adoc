= link:https://asciidoctor.org/[AsciiDoctor]
:author: Stéphane Hulard
:copyright: CH Studio
:docdate: 2022-10
:lang: fr-FR

== Resources

* https://docs.asciidoctor.org/
* xref:../languages/asciidoc.adoc[Asciidoc language]
