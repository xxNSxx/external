= PHP
:author: Stéphane Hulard
:copyright: CH Studio
:docdate: 2021-12
:lang: fr-FR

PHP est le langage côté serveur choisit pour développer les projets web de nos
clients.

== Avantages

link:http://php.net/[PHP] est un langage de programmation interprété et donc
fortement dynamique. Il est principalement utilisé aujourd'hui pour développer
des applications web.

Son modèle d'exécution stateless permet d'avoir un risque d'erreur mémoire très
limité car chaque nouvelle requête est complètement autonome et à un cycle de
vie très court.

Depuis la version 7 et l'introduction du typage scalaire, le code écrit
gagne en robustesse et en clarté. La communauté autour du langage est aussi très
active (conférences, articles, …) et de nombreux outils Open Source peuvent être
utilisés pour gérer, analyser et fiabiliser le code (gestion de dépendances,
analyse statique, tests unitaires, fonctionnels, code sniffing, …).

Des initiatives comme le link:https://www.php-fig.org/[PHPFIG (Framework Interoperability Group)]
fédèrent et normalisent la façon d'écrire du PHP pour permettre une grande
interopérabilité entre les outils et librairies.

Les frameworks disponibles tels que link:https://symfony.com[Symfony] et
link:https://laravel.com[Laravel] ont largement fait leurs preuves et sont reconnus
mondialement pour leur robustesse.

Sur le marché, il y a aussi un grand nombre de développeurs hautements qualifiés
qui ont été formés à l'utilisation de ces outils.
