= SQL
:author: Stéphane Hulard
:copyright: CH Studio
:docdate: 2022-07
:lang: fr-FR

SQL ou **S**tructure **Q**uery **L**anguage est un langage de requête utilisé
principalement dans les bases de données relationnelles. Ce langage est "https://fr.wikipedia.org/wiki/Turing-complet[Turing Complete]" en utilisant https://wiki.postgresql.org/wiki/Turing_Machine_(with_recursive)[certaines fonctionnalités récentes]


[quote, Wikipedia, https://fr.wikipedia.org/wiki/Structured_Query_Language]
Créé en 1974, normalisé depuis 1986, le langage est reconnu par la grande
majorité des systèmes de gestion de bases de données relationnelles
(abrégé SGBDR) du marché.

== Fonctionnalités spécifiques

=== CTE (Common Table Expressions)

Le mot clé `WITH` permet de créer une sorte de table temporaire dans laquelle
il est possible de requêter. On peut plus facilement découper la logique de
requête et gagner en clarté. On peut aussi aller plus loin pour recouper des
jeux de données.

Un petit exemple :

[source,sql]
----
-- Retourne la liste des paiements pour lesquels l'utilisateur n'a pas d'email
WITH
    users_without_emails (userId, userFullName) AS # <1>
    (
        SELECT id as userId, CONCAT(firstname, ' ', lastname) as userFullName # <2>
        FROM users
        WHERE email IS NULL
    )
SELECT uwe.userFullName, p.id, p.amount # <4>
FROM payments p
INNER JOIN users_without_emails uwe ON p.user_id = uwe.userId # <3>
----
<1> On nomme la "vue" en exposant les champs, par défaut tout les champs de la
requête ressortent.
<2> Définie la requête pour la construction de la vue.
<3> Jointure avec la "vue" et intégration au résultat.
<4> On peut utiliser les champs de la CTE pour les intégrer au résultat final.

WARNING: N'est pas compatible avec MySQL 5

==== Liens

* https://modern-sql.com/feature/with
* https://dev.mysql.com/doc/refman/8.0/en/with.html

== Ressources

* https://use-the-index-luke.com/
* https://modern-sql.com
* https://l_avrot.gitlab.io/slides/sql_20190515.html#/
