.DEFAULT_GOAL := build

ASCII_DOCTOR_BIN?=docker run --rm -it -w /documents -v `pwd`:/documents/ asciidoctor/docker-asciidoctor asciidoctor

build: clean ## Transform adoc files to HTML
	$(ASCII_DOCTOR_BIN) -D build -R . '**/*.adoc'
.PHONY: build

clean: ## Clean built files
	rm -rf ./build
.PHONY: clean
